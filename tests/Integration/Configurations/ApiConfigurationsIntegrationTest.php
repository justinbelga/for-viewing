<?php

namespace Tests\Integration\Configurations;

use AES\Api\Configurations\Configuration;
use AES\Api\Users\User;
use Tests\Integration\AdminIntegrationTestCase;
use Tests\TestCase;

class ApiConfigurationsIntegrationTest extends AdminIntegrationTestCase
{

    /** @test */
    public function it_errors_if_the_send_mode_is_invalid()
    {
        $user = factory(User::class)->create();

        $data = [
            'send_interval' => $this->faker->numberBetween(1, 60),
            'send_mode' => "invalid_value",
            'send_by_trans_status' => $this->faker->boolean,
            'can_send_specific_ta' => $this->faker->boolean,
            'request_expires_when_missed' => $this->faker->boolean,
            'user_id' => $user->id,
        ];

        $this->post(route('configurations.index'), $data)
            ->assertStatus(422)
            ->assertJsonFragment(
                [
                    'error' => [
                        'send_mode' => [
                            0 => "The selected send mode is invalid."
                        ]
                    ]
                ]
            );
    }

    /** @test */
    public function it_errors_if_the_send_interval_is_invalid()
    {
        $user = factory(User::class)->create();

        $data = [
            'send_interval' => 70,
            'send_mode' => $this->faker->randomElement(['random', 'sequential', 'at_once']),
            'send_by_trans_status' => $this->faker->boolean,
            'can_send_specific_ta' => $this->faker->boolean,
            'request_expires_when_missed' => $this->faker->boolean,
            'user_id' => $user->id,
        ];

        $this->post(route('configurations.index'), $data)->assertStatus(422)
        ->assertJsonFragment(
            [
                'error' => [
                    'send_interval' => [
                        0 => "The send interval must be between 1 and 60."
                    ]
                ]
            ]
        );
    }

    /** @test */
    public function it_errors_if_required_field_is_missing_during_create_request()
    {
        $this->post(route('configurations.index'),
            [
                'send_by_trans_status' => $this->faker->boolean,
                'can_send_specific_ta' => $this->faker->boolean,
            ]
        )
            ->assertStatus(422)
            ->assertJsonFragment([
                'error' => [
                    'send_mode' => [
                        0 => 'The send mode field is required.'
                    ],
                    'user_id' => [
                        0 => 'The user id field is required.'
                    ]
                ]
            ]);
    }

    /** @test */
    public function it_can_create_configuration()
    {
        $user = factory(User::class)->create();

        $data = [
            'send_interval' => $this->faker->numberBetween(1, 60),
            'send_mode' => $this->faker->randomElement(['random', 'sequential', 'at_once']),
            'send_by_trans_status' => $this->faker->boolean,
            'can_send_specific_ta' => $this->faker->boolean,
            'request_expires_when_missed' => $this->faker->boolean,
            'user_id' => $user->id,
        ];

        $this->post(route('configurations.store'), $data)
            ->assertStatus(201)
            ->assertJson($data);
    }

    /** @test */
    public function it_can_update_the_configuration()
    {
        $configuration = factory(Configuration::class)->create();
        $user = factory(User::class)->create();

        $data = [
            'send_interval' => $this->faker->numberBetween(1, 60),
            'send_mode' => $this->faker->randomElement(['random', 'sequential', 'at_once']),
            'send_by_trans_status' => $this->faker->boolean,
            'can_send_specific_ta' => $this->faker->boolean,
            'request_expires_when_missed' => $this->faker->boolean,
            'user_id' => $user->id,
        ];

        $this->put(route('configurations.update', $configuration->id), $data)
            ->assertStatus(200)
            ->assertJson($data);
    }

    /** @test */
    public function it_can_show_the_configuration()
    {
        $configuration = factory(Configuration::class)->create();

        $this->get(route('configurations.show', ['id' => $configuration->id]))
            ->assertStatus(200)
            ->assertJson([
                'send_interval' => $configuration->send_interval,
                'send_mode' => $configuration->send_mode,
                'send_by_trans_status' => $configuration->send_by_trans_status,
                'can_send_specific_ta' => $configuration->can_send_specific_ta,
                'request_expires_when_missed' => $configuration->request_expires_when_missed,
                'user_id' => $configuration->user_id,
            ]);
    }

    /** @test */
    public function it_should_return_all_configurations()
    {
        $configuration = factory(Configuration::class)->create();

        $this->get(route('configurations.index'))
            ->assertStatus(200)
            ->assertJsonFragment([
                'send_interval' => (string) $configuration->send_interval,
                'send_mode' => $configuration->send_mode,
                'send_by_trans_status' => (string) intVal($configuration->send_by_trans_status),
                'can_send_specific_ta' => (string) intVal($configuration->can_send_specific_ta),
                'request_expires_when_missed' => (string) intval($configuration->request_expires_when_missed),
                'user_id' => (string) $configuration->user_id,
            ]);
    }

    /** @test */
    public function it_can_delete_the_configuration()
    {
        $configuration = factory(Configuration::class)->create();

        $this->delete(route('configurations.destroy', ['id' => $configuration->id]))
            ->assertStatus(202)
            ->assertJson(['message' => 'Configuration deleted.']);
    }
}