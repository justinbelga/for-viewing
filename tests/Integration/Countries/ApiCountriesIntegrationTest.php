<?php

namespace Tests\Integration\Countries;

use AES\Api\Countries\Country;
use function GuzzleHttp\Psr7\str;
use Tests\Integration\AdminIntegrationTestCase;
use Tests\TestCase;

class ApiCountriesIntegrationTest extends AdminIntegrationTestCase
{
    /** @test */
    public function it_errors_if_required_field_is_missing_during_create_request()
    {
        $this->post(route('countries.index'), [
            'iso3' => 'Sample iso3',
        ])->assertStatus(422)
            ->assertJsonFragment([
                'error' => [
                    'name' => [
                        0 => 'The name field is required.'
                    ],
                    'iso' => [
                        0 => 'The iso field is required.'
                    ],
                    'phonecode' => [
                        0 => 'The phonecode field is required.'
                    ]
                ]
            ]);
    }

    /** @test */
    public function it_can_create()
    {
        $this->post(route('countries.store'), [
            'name' => $this->faker->country,
            'iso' => $this->faker->word,
            'iso3' => $this->faker->word,
            'numcode' => $this->faker->randomDigitNotNull,
            'phonecode' => $this->faker->randomDigit,
        ])->assertStatus(201);
    }

    /** @test */
    public function it_can_update_the_country()
    {
        $country = factory(Country::class)->create();

        $data = [
            'name' => $this->faker->country,
            'iso' => $this->faker->word,
            'iso3' => $this->faker->word,
            'numcode' => $this->faker->randomDigitNotNull,
            'phonecode' => $this->faker->randomDigit,
            'status' => 1
        ];

        $this->put(route('countries.update', ['id' => $country->id]), $data)
            ->assertStatus(200)
            ->assertJson([
                'name' => $data['name'],
                'iso' => $data['iso'],
                'iso3' => $data['iso3'],
                'numcode' => $data['numcode'],
                'phonecode' => $data['phonecode'],
                'status' => $data['status']
            ]);
    }

    /** @test */
    public function it_errors_when_name_is_not_unique()
    {
        $country1 = factory(Country::class)->create();
        $country2 = factory(Country::class)->create();

        $data = [
            'name' => $country2->name,
            'iso' => $this->faker->word,
            'iso3' => $this->faker->word,
            'numcode' => $this->faker->randomDigitNotNull,
            'phonecode' => $this->faker->randomDigit,
            'status' => 1
        ];

        $this->put(route('countries.update', $country1->id), $data)
            ->assertStatus(422)
            ->assertJsonFragment([
                'error' => [
                    'name' => [
                        'The name has already been taken.'
                    ]
                ]
            ]);
    }

    /** @test */
    public function it_can_show_the_country()
    {
        $country = factory(Country::class)->create();

        $this->get(route('countries.show', ['id' => $country->id]))
            ->assertStatus(200)
            ->assertJson([
                'name' => $country->name,
                'iso' => $country->iso,
                'iso3' => $country->iso3,
                'numcode' => $country->numcode,
                'phonecode' => $country->phonecode,
                'status' => $country->status
            ]);
    }

    /** @test */
    public function it_should_return_all_countries()
    {
        $country = factory(Country::class)->create();

        $this->get(route('countries.index'))
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => $country->name,
                'iso' => (string)$country->iso,
                'iso3' => (string)$country->iso3,
                'numcode' => (string)$country->numcode,
                'phonecode' => (string)$country->phonecode,
                'status' => (string)$country->status
            ]);
    }

    /** @test */
    public function it_can_delete_the_country()
    {
        $country = factory(Country::class)->create();

        $this->delete(route('countries.destroy', ['id' => $country->id]))
            ->assertStatus(202)
            ->assertJson(['message' => 'Country deleted.']);
    }
}