<?php

namespace Tests\Integration;

use AES\Api\Permissions\Permission;
use AES\Api\Users\Repositories\UserRepository;
use AES\Api\Users\User;
use AES\Roles\Repositories\RoleRepository;
use AES\Roles\Role;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\CreatesApplication;
use Tests\TestCase;
use Faker\Factory as Faker;

class SuperAdminIntegrationTestCase extends TestCase
{
    use CreatesApplication, DatabaseMigrations, DatabaseTransactions;

    protected $faker;

    /**
     * Set up the test
     */
    public function setUp()
    {
        parent::setUp();
        $this->faker = Faker::create();

        $details = [
            'name' => $this->faker->name,
            'email' => $this->faker->companyEmail,
            'password' => bcrypt($this->faker->uuid)
        ];

        $user = factory(User::class)->create($details);
        $repo = new UserRepository($user);

        $role = factory(Role::class)->create(['name' => 'super_admin']);
        $repo->attachRole($role);

        $perms = [
            'create-user',
            'find-user',
            'update-user',
            'delete-user'
        ];

        foreach ($perms as $key => $value) {
            $perm = factory(Permission::class)->create(['name' => $value]);
            $role->attachPermission($perm);
        }

        Passport::actingAs($repo->findUserById($user->id));
    }

    /**
     * Reset the migrations
     */
    public function tearDown()
    {
        $this->artisan('migrate:reset');
        parent::tearDown();
    }
}