<?php

namespace Tests\Integration\Departments;

use AES\Api\Companies\Company;
use AES\Api\Departments\Department;
use Tests\Integration\AdminIntegrationTestCase;

class ApiDepartmentsIntegrationTest extends AdminIntegrationTestCase
{
    /** @test */
    public function it_errors_if_required_field_is_missing_during_create_request()
    {
        $this->post(route('departments.store'), [
            'mobile' => $this->faker->e164PhoneNumber,
            'address_2' => $this->faker->address,
        ])->assertStatus(422)
            ->assertJsonFragment([
                'error' => [
                    'name' => [
                        'The name field is required.'
                    ],
                    'email' => [
                        'The email field is required.'
                    ],
                    'phone' => [
                        'The phone field is required.'
                    ],
                    'address' => [
                        'The address field is required.'
                    ],
                    'company_id' => [
                        'The company id field is required.'
                    ]
                ]
            ]);
    }

    /** @test */
    public function it_can_create()
    {
        $company = factory(Company::class)->create();

        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone' => $this->faker->phoneNumber,
            'mobile' => $this->faker->e164PhoneNumber,
            'address' => $this->faker->address,
            'address_2' => $this->faker->address,
            'company_id' => $company->id
        ];

        $this->post(route('departments.store'), $data)
            ->assertStatus(201)
            ->assertJson($data);
    }

    /** @test */
    public function it_can_update_the_department()
    {
        $company_1 = factory(Company::class)->create();
        $company_2 = factory(Company::class)->create();

        $department = factory(Department::class)->create(['company_id' => $company_1->id]);

        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone' => $this->faker->phoneNumber,
            'mobile' => $this->faker->e164PhoneNumber,
            'address' => $this->faker->address,
            'address_2' => $this->faker->address,
            'company_id' => $company_2->id
        ];

        $this->put(route('departments.update', ['id' => $department->id]), $data)
            ->assertStatus(200)
            ->assertJson($data);
    }

    /** @test */
    public function it_can_show_the_department()
    {
        $department = factory(Department::class)->create();

        $this->get(route('departments.show', ['id' => $department->id]))
            ->assertStatus(200)
            ->assertJson([
                'name' => $department->name,
                'email' => $department->email,
            ]);
    }

    /** @test */
    public function it_should_return_all_departments()
    {
        $department = factory(Department::class)->create();

        $this->get(route('departments.index'))
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => $department->name,
                'email' => $department->email,
            ]);
    }

    /** @test */
    public function it_can_delete_the_department()
    {
        $department = factory(Department::class)->create();

        $this->delete(route('departments.destroy', ['id' => $department->id]))
            ->assertStatus(202)
            ->assertJson(['message' => 'Department deleted.']);
    }
}