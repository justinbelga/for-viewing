<?php

namespace Tests\Integration\Municipalities;

use AES\Api\Municipalities\Municipality;
use function GuzzleHttp\Psr7\str;
use Tests\Integration\AdminIntegrationTestCase;
use Tests\TestCase;

class ApiMunicipalitiesIntegrationTest extends AdminIntegrationTestCase
{
    /** @test */
    public function it_errors_if_required_field_is_missing_during_create_request()
    {
        $this->post(route('municipalities.store'), [
            'code' => $this->faker->randomDigit,
        ])->assertStatus(422)
            ->assertJson([
                'error' => [
                    'name' => [
                        'The name field is required.'
                    ]
                ]
            ]);
    }

    /** @test */
    public function it_can_create()
    {
        $data = [
            'name' => $this->faker->city,
            'code' => $this->faker->randomDigit
        ];

        $this->post(route('municipalities.store'), $data)
            ->assertStatus(201)
            ->assertJson($data);
    }

    /** @test */
    public function it_can_update_the_municipality()
    {
        $municipality = factory(Municipality::class)->create();
        $data = [
            'name' => $this->faker->city,
            'code' => $this->faker->randomDigit,
        ];

        $this->put(route('municipalities.update', ['id' => $municipality->id]), $data)
            ->assertStatus(200)
            ->assertJson($data);
    }

    /** @test */
    public function it_can_show_the_municipality()
    {
        $municipality = factory(Municipality::class)->create();

        $this->get(route('municipalities.show', ['id' => $municipality->id]))
            ->assertStatus(200)
            ->assertJson([
                'name' => $municipality->name,
            ]);
    }

    /** @test */
    public function it_should_return_all_municipalities()
    {
        $municipality = factory(Municipality::class)->create();

        $this->get(route('municipalities.index'))
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => $municipality->name,
                'code' => (string)$municipality->code,
            ]);
    }

    /** @test */
    public function it_can_delete_the_municipality()
    {
        $municipality = factory(Municipality::class)->create();

        $this->delete(route('municipalities.destroy', ['id' => $municipality->id]))
            ->assertStatus(202)
            ->assertJson(['message' => 'Municipality deleted.']);
    }
}