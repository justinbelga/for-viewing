<?php

namespace Tests\Integration\Companies;

use AES\Api\Companies\Company;
use Tests\Integration\AdminIntegrationTestCase;
use Tests\Integration\Roles\SuperAdmin\SuperAdminIntegrationTestCase;
use Tests\TestCase;

class ApiCompaniesIntegrationTest extends AdminIntegrationTestCase
{
    /** @test */
    public function it_errors_if_required_field_is_missing_during_create_request()
    {
        $data = [
            'country' => $this->faker->country,
            'post_code' => $this->faker->postcode,
        ];

        $this->post(route('companies.store'), $data)
            ->assertStatus(422)
            ->assertJson([
                'error' => [
                    'name' => [
                        'The name field is required.'
                    ],
                    'email' => [
                        'The email field is required.'
                    ],
                    'phone' => [
                        'The phone field is required.'
                    ],
                    'mobile' => [
                        'The mobile field is required.'
                    ],
                    'address' => [
                        'The address field is required.'
                    ]
                ]
            ]);
    }

    /** @test */
    public function it_can_create()
    {
        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone' => $this->faker->phoneNumber,
            'mobile' => $this->faker->e164PhoneNumber,
            'address' => $this->faker->address,
        ];

        $this->post(route('companies.store'), $data)
            ->assertStatus(201)
            ->assertJson($data);
    }

    /** @test */
    public function it_can_update_the_company()
    {
        $company = factory(Company::class)->create();

        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone' => $this->faker->phoneNumber,
            'mobile' => $this->faker->e164PhoneNumber,
            'address' => $this->faker->address,
        ];

        $this->put(route('companies.update', ['id' => $company->id]), $data)
            ->assertStatus(200)
            ->assertJson($data);
    }

    /** @test */
    public function it_can_show_the_company()
    {
        $company = factory(Company::class)->create();

        $this->get(route('companies.show', ['id' => $company->id]))
            ->assertStatus(200)
            ->assertJson([
                'name' => $company->name,
                'email' => (string)$company->email,
                'phone' => $company->phone,
                'mobile' => $company->mobile,
                'address' => $company->address,
            ]);
    }

    /** @test */
    public function it_should_return_all_companies()
    {
        $company = factory(Company::class)->create();

        $this->get(route('companies.index'))
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => $company->name,
                'email' => (string)$company->email,
                'phone' => $company->phone,
                'mobile' => $company->mobile,
                'address' => $company->address,
            ]);
    }

    /** @test */
    public function it_can_delete_the_company()
    {
        $company = factory(Company::class)->create();

        $this->delete(route('companies.destroy', ['id' => $company->id]))
            ->assertStatus(202)
            ->assertJson(['message' => 'Company deleted.']);
    }
}