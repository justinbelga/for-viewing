<?php

namespace Tests\Integration;

use AES\Api\Users\Repositories\UserRepository;
use AES\Api\Users\User;
use AES\Roles\Role;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\CreatesApplication;
use Tests\TestCase;
use Faker\Factory as Faker;

class IntegrationTestCase extends TestCase
{
    use CreatesApplication, DatabaseMigrations, DatabaseTransactions;

    protected $faker;
    protected $header;
    protected $user;

    /**
     * Set up the test
     */
    public function setUp()
    {
        parent::setUp();
        $this->faker = Faker::create();

        $details = [
            'name' => $this->faker->name,
            'email' => $this->faker->companyEmail,
            'password' => bcrypt($this->faker->uuid)
        ];

        $this->user = factory(User::class)->create($details);
        $repo = new UserRepository($this->user);

        $role = factory(Role::class)->create(['name' => 'regular']);
        $repo->attachRole($role);

        Passport::actingAs($this->user);
    }

    /**
     * Reset the migrations
     */
    public function tearDown()
    {
        $this->artisan('migrate:reset');
        parent::tearDown();
    }
}