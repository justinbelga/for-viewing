<?php

namespace Tests\Unit\Departments;

use AES\Api\Departments\Department;
use AES\Api\Departments\Exceptions\CreateDepartmentInvalidArgumentException;
use AES\Api\Departments\Exceptions\DepartmentNotFoundException;
use AES\Api\Departments\Exceptions\UpdateDepartmentInvalidArgumentException;
use AES\Api\Departments\Repositories\DepartmentRepository;
use Tests\TestCase;

class DepartmentTest extends TestCase
{
    /** @test */
    public function it_can_delete_the_department()
    {
        $department = factory(Department::class)->create();

        $this->assertDatabaseHas('departments', ['name' => $department->name]);

        $departmentRepo = new DepartmentRepository($department);
        $departmentRepo->deleteDepartment();

        $this->assertDatabaseMissing('departments', ['name' => $department->name]);
    }

    /** @test */
    public function it_errors_when_looking_for_a_department_that_is_not_found()
    {
        $this->expectException(DepartmentNotFoundException::class);
        $this->expectExceptionMessage('Problem locating the department');

        $departmentRepo = new DepartmentRepository(new Department);
        $departmentRepo->findDepartmentById(999);
    }

    /** @test */
    public function it_can_find_the_department()
    {
        $department = factory(Department::class)->create();

        $departmentRepo = new DepartmentRepository(new Department);
        $found = $departmentRepo->findDepartmentById($department->id);

        $this->assertInstanceOf(Department::class, $department);
        $this->assertEquals($department->name, $found->name);
        $this->assertEquals($department->email, $found->email);
        $this->assertEquals($department->phone, $found->phone);
        $this->assertEquals($department->mobile, $found->mobile);
        $this->assertEquals($department->address, $found->address);
        $this->assertEquals($department->address_2, $found->address_2);
    }

    /** @test */
    public function it_errors_when_updating_required_fields_as_null()
    {
        $this->expectException(UpdateDepartmentInvalidArgumentException::class);
        $this->expectExceptionMessage('Problem updating the department');

        $department = factory(Department::class)->create();

        $data = [
            'name' => null
        ];

        $departmentRepo = new DepartmentRepository($department);
        $departmentRepo->updateDepartment($data);
    }

    /** @test */
    public function it_can_update_the_department()
    {
        $department = factory(Department::class)->create();

        $data = [
            'name' => $this->faker->word
        ];

        $departmentRepo = new DepartmentRepository($department);
        $updated = $departmentRepo->updateDepartment($data);

        $this->assertTrue($updated);
        $this->assertDatabaseHas('departments', ['name' => $data['name']]);
    }

    /** @test */
    public function it_errors_when_creating_the_department()
    {
        $this->expectException(CreateDepartmentInvalidArgumentException::class);
        $this->expectExceptionMessage('Problem creating the department');

        $data = [
            'name' => $this->faker->word
        ];

        $departmentRepo = new DepartmentRepository(new Department);
        $departmentRepo->createDepartment($data);
    }

    /** @test */
    public function it_can_create_the_department()
    {
        $data = [
            'name' => $this->faker->word,
            'email' => $this->faker->email,
            'phone' => $this->faker->phoneNumber,
            'mobile' => $this->faker->phoneNumber,
            'address' => $this->faker->streetAddress,
            'address_2' => null
        ];

        $departmentRepo = new DepartmentRepository(new Department);
        $department = $departmentRepo->createDepartment($data);

        $this->assertInstanceOf(Department::class, $department);
        $this->assertEquals($data['name'], $department->name);
        $this->assertEquals($data['email'], $department->email);
        $this->assertEquals($data['phone'], $department->phone);
        $this->assertEquals($data['mobile'], $department->mobile);
        $this->assertEquals($data['address'], $department->address);
        $this->assertEquals($data['address_2'], $department->address_2);
    }
}