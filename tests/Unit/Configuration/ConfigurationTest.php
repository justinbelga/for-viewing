<?php

namespace Tests\Unit;

use AES\Api\Configurations\Configuration;
use AES\Api\Configurations\Exceptions\ConfigurationInvalidArgumentException;
use AES\Api\Configurations\Exceptions\ConfigurationNotFoundException;
use AES\Api\Configurations\Repositories\ConfigurationRepository;
use AES\Api\Users\User;
use Tests\TestCase;

class ConfigurationTest extends TestCase
{
    /** @test */
    public function it_can_delete_a_configuration()
    {
        $configuration = factory(Configuration::class)->create();

        $configurationRepo = new ConfigurationRepository($configuration);
        $configurationRepo->deleteConfiguration();

        $this->assertDatabaseMissing('configurations', ['id' => $configuration->id]);
    }

    /** @test */
    public function it_errors_when_the_configuration_is_not_found()
    {
        $this->expectException(ConfigurationNotFoundException::class);
        $this->expectExceptionMessage('Problem locating the configuration.');

        $configurationRepo = new ConfigurationRepository(new Configuration);
        $configurationRepo->findConfiguration(999);
    }

    /** @test */
    public function it_can_get_a_specific_configuration()
    {
        $configuration = factory(Configuration::class)->create();

        $configurationRepo = new ConfigurationRepository(new Configuration);
        $found = $configurationRepo->findConfiguration($configuration->id);

        $this->assertInstanceOf(Configuration::class, $configuration);
        $this->assertEquals($configuration->send_mode, $found->send_mode);
        $this->assertEquals($configuration->send_intervals, $found->send_intervals);
    }

    /** @test */
    public function it_errors_when_the_required_fields_are_not_set_when_updating_a_configuration()
    {
        $this->expectException(ConfigurationInvalidArgumentException::class);
        $this->expectExceptionMessage('Problem updating the configuration.');

        $user = factory(User::class)->create();

        $data = [
            'send_mode' => null,
            'send_by_trans_status' => $this->faker->randomDigit,
            'can_send_specific_ta' => $this->faker->boolean,
            'user_id' => $user->id,
        ];

        $configuration = factory(Configuration::class)->create();

        $configurationRepo = new ConfigurationRepository($configuration);
        $configurationRepo->updateConfiguration($data);
    }

    /** @test */
    public function it_can_update_a_configuration()
    {
        $user = factory(User::class)->create();

        $data = [
            'send_interval' => $this->faker->numberBetween(1, 60),
            'send_mode' => $this->faker->randomElement(['random', 'sequential', 'at_once']),
            'send_by_trans_status' => $this->faker->boolean,
            'can_send_specific_ta' => $this->faker->boolean,
            'request_expires_when_missed' => $this->faker->boolean,
            'user_id' => $user->id,
        ];

        $configuration = factory(Configuration::class)->create();

        $configurationRepo = new ConfigurationRepository($configuration);
        $updated = $configurationRepo->updateConfiguration($data);

        $this->assertTrue($updated);
        $this->assertEquals($data['send_mode'], $configuration->send_mode);
        $this->assertEquals($data['send_interval'], $configuration->send_interval);
    }

    /** @test */
    public function it_errors_when_the_required_fields_are_not_passed_when_creating_a_configuration()
    {
        $this->expectException(ConfigurationInvalidArgumentException::class);
        $this->expectExceptionMessage('Problem creating the configuration.');

        $user = factory(User::class)->create();

        $data = [
            'send_by_trans_status' => $this->faker->boolean,
            'can_send_specific_ta' => $this->faker->boolean,
            'request_expires_when_missed' => $this->faker->boolean,
            'user_id' => $user->id,
        ];

        $configurationRepo = new ConfigurationRepository(new Configuration);
        $configurationRepo->createConfiguration($data);
    }

    /** @test */
    public function it_can_create_a_configuration()
    {
        $user = factory(User::class)->create();

        $data = [
            'send_interval' => $this->faker->numberBetween(1, 60),
            'send_mode' => $this->faker->randomElement(['random', 'sequential', 'at_once']),
            'send_by_trans_status' => $this->faker->boolean,
            'can_send_specific_ta' => $this->faker->boolean,
            'request_expires_when_missed' => $this->faker->boolean,
            'user_id' => $user->id,
        ];

        $configurationRepo = new ConfigurationRepository(new Configuration);
        $configuration = $configurationRepo->createConfiguration($data);

        $this->assertInstanceOf(Configuration::class, $configuration);
        $this->assertEquals($data['send_mode'], $configuration->send_mode);
        $this->assertEquals($data['send_interval'], $configuration->send_interval);
    }
}
