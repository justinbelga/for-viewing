<?php

namespace Tests\Unit\Country\Municipalities;

use AES\Api\Municipalities\Exceptions\CreateMunicipalityInvalidArgumentException;
use AES\Api\Municipalities\Exceptions\MunicipalityNotFoundException;
use AES\Api\Municipalities\Exceptions\UpdateMunicipalityInvalidArgumentException;
use AES\Api\Municipalities\Municipality;
use AES\Api\Municipalities\Repositories\MunicipalityRepository;
use Tests\TestCase;

class MunicipalityTest extends TestCase
{
    /** @test */
    public function it_can_delete_the_municipality()
    {
        $createdMunicipality = factory(Municipality::class)->create();

        $this->assertDatabaseHas('municipalities', ['name' => $createdMunicipality->name]);

        $municipalityRepo = new MunicipalityRepository($createdMunicipality);
        $deleted = $municipalityRepo->deleteMunicipality();

        $this->assertTrue($deleted);
        $this->assertDatabaseMissing('municipalities', ['name' => $createdMunicipality->name]);
    }

    /** @test */
    public function it_errors_when_looking_for_a_municipality_that_is_not_found()
    {
        $this->expectException(MunicipalityNotFoundException::class);
        $this->expectExceptionMessage('Municipality not found');

        $municipalityRepo = new MunicipalityRepository(new Municipality);
        $municipalityRepo->findMunicipality(999);
    }
    
    /** @test */
    public function it_can_find_the_municipality()
    {
        $created = factory(Municipality::class)->create();

        $municipalityRepo = new MunicipalityRepository($created);
        $found = $municipalityRepo->findMunicipality($created->id);

        $this->assertInstanceOf(Municipality::class, $found);
        $this->assertEquals($created->name, $found->name);
        $this->assertEquals($created->code, $found->code);
    }

    /** @test */
    public function it_errors_updating_the_municipality_with_required_fields_as_null()
    {
        $this->expectException(UpdateMunicipalityInvalidArgumentException::class);
        $this->expectExceptionMessage('Problem updating the municipality');

        $createdMunicipality = factory(Municipality::class)->create();

        $data = [
            'name' => null
        ];

        $municipalityRepo = new MunicipalityRepository($createdMunicipality);
        $municipalityRepo->updateMunicipality($data);
    }

    /** @test */
    public function it_can_update_the_municipality()
    {
        $municipality = factory(Municipality::class)->create();

        $data = [
            'name' => $this->faker->sentence,
            'code' => $this->faker->randomDigit
        ];

        $municipalityRepo = new MunicipalityRepository($municipality);
        $updated = $municipalityRepo->updateMunicipality($data);

        $this->assertTrue($updated);
        $this->assertEquals($data['name'], $municipality->name);
        $this->assertEquals($data['code'], $municipality->code);
    }

    /** @test */
    public function it_errors_when_creating_the_municipal_with_required_fields_missing()
    {
        $this->expectException(CreateMunicipalityInvalidArgumentException::class);
        $this->expectExceptionMessage('Problem creating the municipality');

        $data = [
            'code' => $this->faker->randomDigit
        ];

        $municipalityRepo = new MunicipalityRepository(new Municipality);
        $municipalityRepo->createMunicipality($data);
    }
    
    /** @test */
    public function it_can_create_a_municipality()
    {
        $data = [
            'name' => $this->faker->sentence,
            'code' => $this->faker->randomDigit
        ];

        $municipalityRepo = new MunicipalityRepository(new Municipality);
        $municipal = $municipalityRepo->createMunicipality($data);

        $this->assertInstanceOf(Municipality::class, $municipal);
        $this->assertEquals($data['name'], $municipal->name);
        $this->assertEquals($data['code'], $municipal->code);
    }
}