<?php

namespace Tests\Unit;

use AES\Api\Countries\Country;
use AES\Api\Countries\Exceptions\CountryInvalidArgumentException;
use AES\Api\Countries\Exceptions\CountryNotFoundException;
use AES\Api\Countries\Repositories\CountryRepository;
use Tests\TestCase;

class CountryTest extends TestCase
{
    /** @test */
    public function it_can_delete_a_country()
    {
        $country = factory(Country::class)->create();

        $countryRepo = new CountryRepository($country);
        $deleted = $countryRepo->deleteCountry();

        $this->assertTrue($deleted);
        $this->assertDatabaseMissing('countries', ['id' => $country->id]);
    }

    /** @test */
    public function it_errors_when_the_country_is_not_found()
    {
        $this->expectException(CountryNotFoundException::class);
        $this->expectExceptionMessage('Problem locating the country.');

        $countryRepo = new CountryRepository(new Country);
        $countryRepo->findCountry(999);
    }

    /** @test */
    public function it_can_get_a_specific_country()
    {
        $country = factory(Country::class)->create();

        $countryRepo = new CountryRepository(new Country);
        $found = $countryRepo->findCountry($country->id);

        $this->assertInstanceOf(Country::class, $country);
        $this->assertEquals($country->name, $found->name);
        $this->assertEquals($country->iso, $found->iso);
    }

    /** @test */
    public function it_errors_when_the_required_fields_are_not_set_when_updating_a_country()
    {
        $this->expectException(CountryInvalidArgumentException::class);
        $this->expectExceptionMessage('Problem updating the country.');

        $data = [
            'name' => null,
            'iso' => $this->faker->randomDigit
        ];

        $country = factory(Country::class)->create();

        $countryRepo = new CountryRepository($country);
        $countryRepo->updateCountry($data);
    }

    /** @test */
    public function it_can_update_a_country()
    {
        $data = [
            'name' => $this->faker->country,
            'iso' => $this->faker->randomDigit
        ];

        $country = factory(Country::class)->create();

        $countryRepo = new CountryRepository($country);
        $updated = $countryRepo->updateCountry($data);

        $this->assertTrue($updated);
        $this->assertEquals($data['name'], $country->name);
        $this->assertEquals($data['iso'], $country->iso);
    }

    /** @test */
    public function it_errors_when_the_required_fields_are_not_passed_when_creating_a_country()
    {
        $this->expectException(CountryInvalidArgumentException::class);
        $this->expectExceptionMessage('Problem creating the country.');

        $data = [
            'iso' => $this->faker->randomDigit,
            'iso3' => $this->faker->randomDigit,
            'numcode' => $this->faker->randomDigit,
            'phonecode' => $this->faker->randomDigit,
            'status' => 1
        ];

        $countryRepo = new CountryRepository(new Country);
        $countryRepo->createCountry($data);
    }

    /** @test */
    public function it_can_create_a_country()
    {
        $data = [
            'name' => $this->faker->country,
            'iso' => $this->faker->randomDigit,
            'iso3' => $this->faker->randomDigit,
            'numcode' => $this->faker->randomDigit,
            'phonecode' => $this->faker->randomDigit,
            'status' => 1
        ];

        $countryRepo = new CountryRepository(new Country);
        $country = $countryRepo->createCountry($data);

        $this->assertInstanceOf(Country::class, $country);
        $this->assertEquals($data['name'], $country->name);
        $this->assertEquals($data['iso'], $country->iso);
        $this->assertEquals($data['iso3'], $country->iso3);
        $this->assertEquals($data['numcode'], $country->numcode);
        $this->assertEquals($data['phonecode'], $country->phonecode);
        $this->assertEquals($data['status'], $country->status);
    }
}
