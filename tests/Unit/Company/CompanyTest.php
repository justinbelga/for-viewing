<?php

namespace Tests\Unit;

use AES\Api\Companies\Company;
use AES\Api\Companies\Exceptions\CompanyInvalidArgumentException;
use AES\Api\Companies\Exceptions\CompanyNotFoundException;
use AES\Api\Companies\Repositories\CompanyRepository;
use Tests\TestCase;

class CompanyTest extends TestCase
{
    /** @test */
    public function it_can_delete_a_company()
    {
        $company = factory(Company::class)->create();

        $companyRepo = new CompanyRepository($company);
        $companyRepo->deleteCompany();

        $this->assertDatabaseMissing('companies', ['id' => $company->id]);
    }

    /** @test */
    public function it_errors_when_the_company_is_not_found()
    {
        $this->expectException(CompanyNotFoundException::class);
        $this->expectExceptionMessage('Problem locating the company.');

        $companyRepo = new CompanyRepository(new Company);
        $companyRepo->findCompany(999);
    }

    /** @test */
    public function it_can_get_a_specific_company()
    {
        $company = factory(Company::class)->create();

        $companyRepo = new CompanyRepository(new Company);
        $found = $companyRepo->findCompany($company->id);

        $this->assertInstanceOf(Company::class, $company);
        $this->assertEquals($company->name, $found->name);
        $this->assertEquals($company->email, $found->email);
    }

    /** @test */
    public function it_errors_when_the_required_fields_are_not_set_when_updating_a_company()
    {
        $this->expectException(CompanyInvalidArgumentException::class);
        $this->expectExceptionMessage('Problem updating the company.');

        $data = [
            'name' => null,
            'email' => $this->faker->email
        ];

        $company = factory(Company::class)->create();

        $companyRepo = new CompanyRepository($company);
        $companyRepo->updateCompany($data);
    }

    /** @test */
    public function it_can_update_a_company()
    {
        $data = [
            'name' => $this->faker->firstName,
            'email' => $this->faker->email
        ];

        $company = factory(Company::class)->create();

        $companyRepo = new CompanyRepository($company);
        $updated = $companyRepo->updateCompany($data);

        $this->assertTrue($updated);
        $this->assertEquals($data['name'], $company->name);
        $this->assertEquals($data['email'], $company->email);
    }

    /** @test */
    public function it_errors_when_the_required_fields_are_not_passed_when_creating_a_company()
    {
        $this->expectException(CompanyInvalidArgumentException::class);
        $this->expectExceptionMessage('Problem creating the company.');

        $data = [
            'name' => $this->faker->firstName,
//            'email' => $this->faker->email, //this is missing so that it will flag an error
            'phone' => $this->faker->randomDigit,
            'mobile' => $this->faker->randomDigit,
            'address' => "test address",
            'address_2' => "test address 2"
        ];

        $companyRepo = new CompanyRepository(new Company);
        $companyRepo->createCompany($data);
    }

    /** @test */
    public function it_can_create_a_company()
    {
        $data = [
            'name' => $this->faker->firstName,
            'email' => $this->faker->email,
            'phone' => $this->faker->randomDigit,
            'mobile' => $this->faker->randomDigit,
            'address' => "test address",
            'address_2' => "test address 2"
        ];

        $companyRepo = new CompanyRepository(new Company);
        $company = $companyRepo->createCompany($data);

        $this->assertInstanceOf(Company::class, $company);
        $this->assertEquals($data['name'], $company->name);
    }
}
