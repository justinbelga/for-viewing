<?php

namespace AES\Api\Departments\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DepartmentNotFoundException extends NotFoundHttpException
{
}