<?php

namespace AES\Api\Departments\Requests;

use AES\Api\Base\BaseFormRequest;

class CreateDepartmentRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['string', 'required'],
            'email' => ['string', 'required'],
            'phone' => ['string', 'required'],
            'mobile' => ['string', 'nullable'],
            'address' => ['string', 'required'],
            'address_2' => ['string', 'nullable'],
            'company_id' => ['integer', 'required']
        ];
    }
}