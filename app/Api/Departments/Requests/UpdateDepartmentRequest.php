<?php

namespace AES\Api\Departments\Requests;

use AES\Api\Base\BaseFormRequest;

class UpdateDepartmentRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['string'],
            'email' => ['string'],
            'phone' => ['string'],
            'mobile' => ['string'],
            'address' => ['string'],
            'address_2' => ['string'],
            'company_id' => ['integer']
        ];
    }
}