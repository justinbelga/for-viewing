<?php

namespace AES\Api\Departments;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
        'name',
        'email',
        'phone',
        'mobile',
        'address',
        'address_2',
        'company_id'
    ];

}