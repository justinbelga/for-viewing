<?php

namespace AES\Api\Departments\Repositories\Interfaces;

use AES\Api\Base\BaseRepositoryInterface;
use AES\Api\Departments\Department;

interface DepartmentRepositoryInterface extends BaseRepositoryInterface
{
    public function createDepartment(array $data) : Department;

    public function updateDepartment(array $data) : bool;

    public function findAllDepartments();

    public function findDepartmentById(int $id) : Department;
}