<?php

namespace AES\Api\Departments\Repositories;

use AES\Api\Base\BaseRepository;
use AES\Api\Departments\Department;
use AES\Api\Departments\Exceptions\CreateDepartmentInvalidArgumentException;
use AES\Api\Departments\Exceptions\DepartmentNotFoundException;
use AES\Api\Departments\Exceptions\UpdateDepartmentInvalidArgumentException;
use AES\Api\Departments\Repositories\Interfaces\DepartmentRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;

class DepartmentRepository extends BaseRepository implements DepartmentRepositoryInterface
{
    protected $model;

    /**
     * @param Department $department
     * @internal param Model $model
     */
    public function __construct(Department $department)
    {
        parent::__construct($department);
        $this->model = $department;
    }

    /**
     * @return mixed
     */
    public function findAllDepartments()
    {
        return $this->model->get();
    }

    /**
     * Create department
     *
     * @param array $data
     * @return Department
     */
    public function createDepartment(array $data) : Department
    {
        try {
            return $this->create($data);
        } catch (QueryException $e) {
            throw new CreateDepartmentInvalidArgumentException('Problem creating the department');
        }
    }

    /**
     * Update the department
     *
     * @param array $data
     * @return bool
     */
    public function updateDepartment(array $data) : bool
    {
        try {
            return $this->model->update($data);
        } catch (QueryException $e) {
            throw new UpdateDepartmentInvalidArgumentException('Problem updating the department');
        }
    }

    /**
     * Find the department by ID
     *
     * @param int $id
     * @return Department
     */
    public function findDepartmentById(int $id) : Department
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new DepartmentNotFoundException('Problem locating the department');
        }
    }

    /**
     * Delete the department
     *
     * @return bool
     */
    public function deleteDepartment() : bool
    {
        return $this->model->delete();
    }
}