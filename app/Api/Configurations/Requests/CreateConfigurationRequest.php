<?php

namespace AES\Api\Configurations\Requests;

use AES\Api\Base\BaseFormRequest;

class CreateConfigurationRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'send_interval' => ['integer', 'between:1,60'],
            'send_mode' => ['required', 'in:random,sequential,at_once'],
            'send_by_trans_status' => ['boolean'],
            'can_send_specific_ta' => ['boolean'],
            'request_expires_when_missed' => ['boolean'],
            'user_id' => ['required', 'integer'],
        ];
    }
}