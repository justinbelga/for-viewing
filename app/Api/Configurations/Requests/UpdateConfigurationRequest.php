<?php

namespace AES\Api\Configurations\Requests;

use AES\Api\Base\BaseFormRequest;

class UpdateConfigurationRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'send_interval' => ['required', 'integer', 'between:1,60'],
            'send_mode' => ['required', 'in:random,sequential,at_once'],
            'send_by_trans_status' => ['required' ,'boolean'],
            'can_send_specific_ta' => ['required', 'boolean'],
            'request_expires_when_missed' => ['required', 'boolean'],
            'user_id' => ['required', 'integer'],
        ];
    }
}