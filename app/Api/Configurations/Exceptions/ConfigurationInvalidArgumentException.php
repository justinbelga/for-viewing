<?php

namespace AES\Api\Configurations\Exceptions;

use InvalidArgumentException;

class ConfigurationInvalidArgumentException extends InvalidArgumentException
{

}