<?php

namespace AES\Api\Configurations\Exceptions;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ConfigurationNotFoundException extends NotFoundHttpException
{

}