<?php

namespace AES\Api\Configurations\Repositories\Interfaces;

use AES\Api\Base\BaseRepositoryInterface;
use AES\Api\Configurations\Configuration;

interface ConfigurationRepositoryInterface extends BaseRepositoryInterface
{
    public function createConfiguration(array $data): Configuration;

    public function findConfiguration(int $id): Configuration;

    public function updateConfiguration(array $data): bool;

    public function deleteConfiguration(): bool;

    public function findAllConfigurations();
}