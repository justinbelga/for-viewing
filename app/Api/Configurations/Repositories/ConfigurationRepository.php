<?php
namespace AES\Api\Configurations\Repositories;

use AES\Api\Base\BaseRepository;
use AES\Api\Configurations\Configuration;
use AES\Api\Configurations\Exceptions\ConfigurationInvalidArgumentException;
use AES\Api\Configurations\Exceptions\ConfigurationNotFoundException;
use AES\Api\Configurations\Repositories\Interfaces\ConfigurationRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use phpDocumentor\Reflection\Types\Boolean;

class ConfigurationRepository extends BaseRepository implements ConfigurationRepositoryInterface
{
    protected $model;

    /**
     * ConfigurationRepository constructor.
     * @param Configuration $configuration
     */
    public function __construct(Configuration $configuration)
    {
        parent::__construct($configuration);
        $this->model = $configuration;
    }

    /**
     * Return paginated configuration
     *
     * @return mixed
     */
    public function findAllConfigurations()
    {
        return $this->model->get();
    }

    /**
     * Create the configuration
     *
     * @param array $data
     * @return Configuration
     * @throws ConfigurationInvalidArgumentException
     */
    public function createConfiguration(array $data) : Configuration
    {
        try {
            return $this->create($data);
        } catch (QueryException $e) {
            throw new ConfigurationInvalidArgumentException('Problem creating the configuration.');
        }
    }

    /**
     * Find the configuration
     *
     * @param int $id
     * @return Configuration
     * @throws ConfigurationNotFoundException
     */
    public function findConfiguration(int $id) : Configuration
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new ConfigurationNotFoundException('Problem locating the configuration.');
        }
    }

    /**
     * Update the configuration
     *
     * @param array $data
     * @return bool
     * @throws ConfigurationInvalidArgumentException
     */
    public function updateConfiguration(array $data) : bool
    {
        try {
            return $this->model->update($data);
        } catch (QueryException $e) {
            throw new ConfigurationInvalidArgumentException('Problem updating the configuration.');
        }
    }

    /**
     * Delete the configuration
     *
     * @return bool
     */
    public function deleteConfiguration() : bool
    {
        return $this->model->delete();
    }
}