<?php

namespace AES\Api\Configurations;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    protected $fillable = [
        'send_interval',
        'send_mode',
        'send_by_trans_status',
        'can_send_specific_ta',
        'request_expires_when_missed',
        'user_id',
    ];

    protected $hidden = [];
}
