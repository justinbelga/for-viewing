<?php

namespace AES\Api\Companies\Exceptions;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CompanyNotFoundException extends NotFoundHttpException
{

}