<?php

namespace AES\Api\Companies\Exceptions;

use InvalidArgumentException;

class CompanyInvalidArgumentException extends InvalidArgumentException
{
}