<?php

namespace AES\Api\Companies\Repositories;

use AES\Api\Base\BaseRepository;
use AES\Api\Companies\Company;
use AES\Api\Companies\Exceptions\CompanyInvalidArgumentException;
use AES\Api\Companies\Exceptions\CompanyNotFoundException;
use AES\Api\Companies\Repositories\Interfaces\CompanyRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Pagination\LengthAwarePaginator;

class CompanyRepository extends BaseRepository implements CompanyRepositoryInterface
{
    protected $model;

    /**
     * CompanyRepository constructor.
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        parent::__construct($company);
        $this->model = $company;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function findAllCompanies()
    {
        return $this->model->get();
    }

    /**
     * Create the company
     *
     * @param array $data
     * @return Company
     * @throws CompanyInvalidArgumentException
     */
    public function createCompany(array $data) : Company
    {
        try {
            return $this->create($data);
        } catch (QueryException $e) {
            throw new CompanyInvalidArgumentException('Problem creating the company.');
        }
    }

    /**
     * Find the company
     *
     * @param int $id
     * @return Company
     * @throws CompanyNotFoundException
     */
    public function findCompany(int $id) : Company
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new CompanyNotFoundException('Problem locating the company.');
        }
    }

    /**
     * Update the company
     *
     * @param array $data
     * @return bool
     * @throws CompanyInvalidArgumentException
     */
    public function updateCompany(array $data) : bool
    {
        try {
            return $this->model->update($data);
        } catch (QueryException $e) {
            throw new CompanyInvalidArgumentException('Problem updating the company.');
        }
    }

    /**
     * Delete the company
     *
     * @return bool
     */
    public function deleteCompany() : bool
    {
        return $this->model->delete();
    }
}