<?php

namespace AES\Api\Companies\Repositories\Interfaces;

use AES\Api\Base\BaseRepositoryInterface;

interface CompanyRepositoryInterface extends BaseRepositoryInterface
{

    public function findAllCompanies();

    public function createCompany(array $data);

    public function findCompany(int $id);

}