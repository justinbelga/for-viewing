<?php

namespace AES\Api\Companies\Requests;

use AES\Api\Base\BaseFormRequest;

class UpdateCompanyRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [ 'string'],
            'email' => [ 'email'],
            'phone' => [ 'string'],
            'mobile' => [ 'string'],
            'address' => [ 'string'],
            'address_2' => ['nullable', 'string'],
            'city' => ['nullable', 'string'],
            'town' => ['nullable', 'string'],
            'country' => ['nullable', 'string'],
            'post_code' => ['nullable', 'string'],
            'organization_number' => ['nullable', 'string'],
            'cost_place' => ['nullable', 'string'],
            'additional_info' => ['nullable', 'text'],
            'fee' => ['nullable', 'in:yes,no'],
            'charge_ob' => ['nullable', 'in:yes,no'],
            'charge_km' => ['nullable', 'in:yes,no'],
            'time_to_charge' => ['nullable', 'integer'],
            'time_to_pay' => ['nullable', 'integer'],
            'maximum_km' => ['nullable', 'integer'],
            'reference_person' => ['nullable', 'string'],
            'payment_terms' => ['nullable', 'integer'],
            'email_invoice' => ['nullable', 'in:yes,no']
        ];
    }
}