<?php

namespace AES\Api\Companies;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $fillable = [
        'name',
        'email',
        'phone',
        'mobile',
        'address',
        'address_2',
        'city',
        'town',
        'country',
        'post_code',
        'organization_number',
        'cost_place',
        'additional_info',
        'fee',
        'charge_ob',
        'charge_km',
        'time_to_charge',
        'time_to_pay',
        'maximum_km',
        'reference_person',
        'payment_terms',
        'email_invoice',
    ];

    protected $hidden = [];
}
