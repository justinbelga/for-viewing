<?php

namespace AES\Api\Base;

use AES\Api\Configurations\Repositories\ConfigurationRepository;
use AES\Api\Configurations\Repositories\Interfaces\ConfigurationRepositoryInterface;
use AES\Api\Companies\Repositories\CompanyRepository;
use AES\Api\Companies\Repositories\Interfaces\CompanyRepositoryInterface;
use AES\Api\Bookings\Repositories\BookingRepository;
use AES\Api\Bookings\Repositories\Interfaces\BookingRepositoryInterface;
use AES\Api\Countries\Repositories\CountryRepository;
use AES\Api\Countries\Repositories\Interfaces\CountryRepositoryInterface;
use AES\Api\Departments\Repositories\DepartmentRepository;
use AES\Api\Departments\Repositories\Interfaces\DepartmentRepositoryInterface;
use AES\Api\Municipalities\Repositories\Interfaces\MunicipalityRepositoryInterface;
use AES\Api\Municipalities\Repositories\MunicipalityRepository;
use AES\Api\OAuth2\Repositories\Interfaces\OAuth2ClientRepositoryInterface;
use AES\Api\OAuth2\Repositories\OAuth2ClientRepository;
use AES\Api\Permissions\Repositories\Interfaces\PermissionRepositoryInterface;
use AES\Api\Permissions\Repositories\PermissionRepository;
use AES\Api\Users\Repositories\Interfaces\UserRepositoryInterface;
use AES\Api\Users\Repositories\UserRepository;
use AES\Roles\Repositories\Interfaces\RoleRepositoryInterface;
use AES\Roles\Repositories\RoleRepository;
use Illuminate\Support\ServiceProvider;

class BaseRepositoryProvider extends ServiceProvider
{
    /**
     * Bind the interface to an implementation repository class
     */
    public function register()
    {
        $this->app->bind(
            CountryRepositoryInterface::class,
            CountryRepository::class
        );

        $this->app->bind(
            UserRepositoryInterface::class,
            UserRepository::class
        );

        $this->app->bind(
            MunicipalityRepositoryInterface::class,
            MunicipalityRepository::class
        );

        $this->app->bind(
            OAuth2ClientRepositoryInterface::class,
            OAuth2ClientRepository::class
        );

        $this->app->bind(
            RoleRepositoryInterface::class,
            RoleRepository::class
        );

        $this->app->bind(
            PermissionRepositoryInterface::class,
            PermissionRepository::class
        );

        $this->app->bind(
            CompanyRepositoryInterface::class,
            CompanyRepository::class
        );

        $this->app->bind(
            BookingRepositoryInterface::class,
            BookingRepository::class
        );
        
        $this->app->bind(
            DepartmentRepositoryInterface::class,
            DepartmentRepository::class
        );
        
        $this->app->bind(
            ConfigurationRepositoryInterface::class,
            ConfigurationRepository::class
        );
    }
}