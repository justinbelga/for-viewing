<?php

namespace AES\Api\Agencies\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AgencyNotFoundException extends NotFoundHttpException
{
}