<?php

namespace AES\Api\Agencies\Exceptions;

class UpdateAgencyInvalidArgumentException extends \InvalidArgumentException
{
}