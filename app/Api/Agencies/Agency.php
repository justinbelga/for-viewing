<?php

namespace AES\Api\Agencies;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agency extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'mobile',
        'description'
    ];

    protected $dates = ['deleted_at'];
}