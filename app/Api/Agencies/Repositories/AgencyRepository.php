<?php

namespace AES\Api\Agencies\Repositories;

use AES\Api\Base\BaseRepository;
use AES\Api\Agencies\Exceptions\CreateAgencyInvalidArgumentException;
use AES\Api\Agencies\Exceptions\AgencyNotFoundException;
use AES\Api\Agencies\Exceptions\UpdateAgencyInvalidArgumentException;
use AES\Api\Agencies\Repositories\Interfaces\AgencyRepositoryInterface;
use AES\Api\Agencies\Agency;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;

class AgencyRepository extends BaseRepository implements AgencyRepositoryInterface
{
    /**
     * SupplierRepository constructor.
     * @param Agency $agency
     */
    public function __construct(Agency $agency)
    {
        parent::__construct($agency);
        $this->model = $agency;
    }

    /**
     * Create the supplier
     *
     * @param array $data
     * @return Agency
     */
    public function createAgency(array $data) : Agency
    {
        try {
            return $this->create($data);
        } catch (QueryException $e) {
            throw new CreateAgencyInvalidArgumentException('Problem creating agency', 500, $e);
        }
    }

    /**
     * Update the supplier
     *
     * @param array $data
     * @return bool
     */
    public function updateAgency(array $data) : bool
    {
        try {
            return $this->model->update($data);
        } catch (QueryException $e) {
            throw new UpdateAgencyInvalidArgumentException('Problem updating agency', 500, $e);
        }
    }

    /**
     * Find the supplier
     *
     * @param int $id
     * @return Agency
     */
    public function findAgencyById(int $id) : Agency
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new AgencyNotFoundException('Problem locating agency', $e);
        }
    }

    /**
     * Delete the supplier
     *
     * @return bool|null
     */
    public function deleteSupplier()
    {
        return $this->model->delete();
    }
}