<?php

namespace AES\Api\Agencies\Repositories\Interfaces;

use AES\Api\Base\BaseRepositoryInterface;
use AES\Api\Agencies\Agency;

interface AgencyRepositoryInterface extends BaseRepositoryInterface
{
    public function createAgency(array $data) : Agency;

    public function updateAgency(array $data) : bool;

    public function findAgencyById(int $id) : Agency;
}