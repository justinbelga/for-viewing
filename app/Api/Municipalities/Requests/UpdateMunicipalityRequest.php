<?php

namespace AES\Api\Municipalities\Requests;

use AES\Api\Base\BaseFormRequest;

class UpdateMunicipalityRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'code' => ['nullable', 'int'],
        ];
    }
}