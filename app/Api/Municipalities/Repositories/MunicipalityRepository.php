<?php

namespace AES\Api\Municipalities\Repositories;

use AES\Api\Base\BaseRepository;
use AES\Api\Municipalities\Exceptions\CreateMunicipalityInvalidArgumentException;
use AES\Api\Municipalities\Exceptions\MunicipalityNotFoundException;
use AES\Api\Municipalities\Exceptions\UpdateMunicipalityInvalidArgumentException;
use AES\Api\Municipalities\Municipality;
use AES\Api\Municipalities\Repositories\Interfaces\MunicipalityRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Pagination\LengthAwarePaginator;

class MunicipalityRepository extends BaseRepository implements MunicipalityRepositoryInterface
{
    /**
     * MunicipalityRepository constructor.
     * @param Municipality $municipality
     */
    public function __construct(Municipality $municipality)
    {
        parent::__construct($municipality);
        $this->model = $municipality;
    }


    /**
     * Return paginated municipality
     * @return mixed
     */
    public function findAllMunicipalities()
    {
        return $this->model->get();
    }

    /**
     * Create the municipal
     *
     * @param array $data
     * @return Municipality
     * @throws CreateMunicipalityInvalidArgumentException
     */
    public function createMunicipality(array $data) : Municipality
    {
        try {
            return $this->create($data);
        } catch (QueryException $e) {
            throw new CreateMunicipalityInvalidArgumentException('Problem creating the municipality');
        }
    }

    /**
     * Update the municipality
     *
     * @param array $data
     * @return boolean
     * @throws
     */
    public function updateMunicipality(array $data) : bool
    {
        try {
            return $this->model->update($data);
        } catch (QueryException $e) {
            throw new UpdateMunicipalityInvalidArgumentException('Problem updating the municipality');
        }
    }

    /**
     * Find the municipality
     *
     * @param int $id
     * @return Municipality
     * @throws ModelNotFoundException
     */
    public function findMunicipality(int $id) : Municipality
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new MunicipalityNotFoundException('Municipality not found');
        }
    }

    /**
     * Delete the municipality
     *
     * @return bool
     */
    public function deleteMunicipality() : bool
    {
        return $this->model->delete();
    }
}