<?php

namespace AES\Api\Municipalities\Repositories\Interfaces;

use AES\Api\Base\BaseRepositoryInterface;
use AES\Api\Municipalities\Municipality;
use Illuminate\Pagination\LengthAwarePaginator;

interface MunicipalityRepositoryInterface extends BaseRepositoryInterface
{
    public function createMunicipality(array $data) : Municipality;

    public function updateMunicipality(array $data) : bool;

    public function findMunicipality(int $id) : Municipality;

    public function deleteMunicipality() : bool;

    public function findAllMunicipalities();
}