<?php

namespace AES\Api\Municipalities\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MunicipalityNotFoundException extends NotFoundHttpException
{
}