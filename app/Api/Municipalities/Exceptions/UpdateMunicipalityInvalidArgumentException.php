<?php

namespace AES\Api\Municipalities\Exceptions;

class UpdateMunicipalityInvalidArgumentException extends \InvalidArgumentException
{
}