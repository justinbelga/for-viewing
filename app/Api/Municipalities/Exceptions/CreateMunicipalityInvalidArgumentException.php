<?php

namespace AES\Api\Municipalities\Exceptions;

class CreateMunicipalityInvalidArgumentException extends \InvalidArgumentException
{
}