<?php

namespace AES\Api\Municipalities;

use Illuminate\Database\Eloquent\Model;

class Municipality extends Model
{
    protected $fillable = [
        'name',
        'code'
    ];

    protected $hidden = [];
}