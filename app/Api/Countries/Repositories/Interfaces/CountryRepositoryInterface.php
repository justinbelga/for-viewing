<?php

namespace AES\Api\Countries\Repositories\Interfaces;

use AES\Api\Base\BaseRepositoryInterface;
use AES\Api\Countries\Country;
use Illuminate\Pagination\LengthAwarePaginator;

interface CountryRepositoryInterface extends BaseRepositoryInterface
{
    public function createCountry(array $data) : Country;

    public function findCountry(int $id) : Country;

    public function updateCountry(array $data) : bool;

    public function deleteCountry() : bool;

    public function findAllCountries();
}