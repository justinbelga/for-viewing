<?php

namespace AES\Api\Countries\Repositories;

use AES\Api\Base\BaseRepository;
use AES\Api\Countries\Country;
use AES\Api\Countries\Exceptions\CountryInvalidArgumentException;
use AES\Api\Countries\Exceptions\CountryNotFoundException;
use AES\Api\Countries\Repositories\Interfaces\CountryRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;

class CountryRepository extends BaseRepository implements CountryRepositoryInterface
{
    /**
     * CountryRepository constructor.
     * @param Country $country
     */
    public function __construct(Country $country)
    {
        parent::__construct($country);
        $this->model = $country;
    }

    /**
     * Return paginated Countries
     *
     * @return mixed
     */
    public function findAllCountries()
    {
        return $this->model->get();
    }

    /**
     * Create the country
     *
     * @param array $data
     * @return Country
     * @throws CountryInvalidArgumentException
     */
    public function createCountry(array $data): Country
    {

        $data['status'] = 1;

        try {
            return $this->create($data);
        } catch (QueryException $e) {
            throw new CountryInvalidArgumentException('Problem creating the country.');
        }
    }

    /**
     * Find the country
     *
     * @param int $id
     * @return Country
     * @throws CountryNotFoundException
     */
    public function findCountry(int $id): Country
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new CountryNotFoundException('Problem locating the country.');
        }
    }

    /**
     * Update the country
     *
     * @param array $data
     * @return boolean
     * @throws CountryInvalidArgumentException
     */
    public function updateCountry(array $data): bool
    {
        try {
            return $this->model->update($data);
        } catch (QueryException $e) {
            throw new CountryInvalidArgumentException('Problem updating the country.');
        }
    }

    /**
     * Delete the country
     *
     * @return bool
     */
    public function deleteCountry(): bool
    {
        return $this->model->delete();
    }
}