<?php

namespace AES\Api\Countries\Exceptions;

use InvalidArgumentException;

class CountryInvalidArgumentException extends InvalidArgumentException
{
}