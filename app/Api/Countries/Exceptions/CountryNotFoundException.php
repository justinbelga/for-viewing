<?php

namespace AES\Api\Countries\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CountryNotFoundException extends NotFoundHttpException
{
}