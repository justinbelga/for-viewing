<?php
/**
 * Created by PhpStorm.
 * User: ALPHA
 * Date: 8/1/2017
 * Time: 11:41 PM
 */

namespace AES\Api\Countries\Requests;

use AES\Api\Base\BaseFormRequest;

class CreateCountryRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'unique:countries'],
            'iso' => ['required', 'string'],
            'iso3' => ['nullable', 'string'],
            'numcode' => ['nullable', 'int'],
            'phonecode' => ['required', 'int']
        ];
    }
}