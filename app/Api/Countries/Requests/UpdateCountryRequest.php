<?php
/**
 * Created by PhpStorm.
 * User: ALPHA
 * Date: 8/2/2017
 * Time: 11:16 PM
 */

namespace AES\Api\Countries\Requests;


use AES\Api\Base\BaseFormRequest;
use Illuminate\Validation\Rule;

class UpdateCountryRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['string', Rule::unique('countries', 'name')->ignore($this->segment(4))],
            'iso' => ['string'],
            'iso3' => ['string'],
            'numcode' => ['int'],
            'phonecode' => ['int'],
            'status' => ['int']
        ];
    }
}