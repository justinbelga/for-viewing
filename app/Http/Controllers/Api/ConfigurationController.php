<?php
namespace AES\Http\Controllers\Api;

use AES\Api\Configurations\Repositories\ConfigurationRepository;
use AES\Api\Configurations\Repositories\Interfaces\ConfigurationRepositoryInterface;
use AES\Api\Configurations\Requests\CreateConfigurationRequest;
use AES\Api\Configurations\Requests\UpdateConfigurationRequest;
use Illuminate\Http\Request;
use AES\Http\Controllers\Controller;

class ConfigurationController extends Controller
{
    private $configurationRepo;
    public function __construct(ConfigurationRepositoryInterface $configurationRepository)
    {
        $this->configurationRepo = $configurationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $configurations = $this->configurationRepo->findAllConfigurations();

        $paginate = $this->configurationRepo->paginateArrayResults(collect($configurations)->toArray());

        return response()->json($paginate);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateConfigurationRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateConfigurationRequest $request)
    {
        $data = $request->all();
        $configuration = $this->configurationRepo->createConfiguration($data);
        return response()->json($configuration, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $configuration = $this->configurationRepo->findConfiguration($id);
        return response()->json($configuration);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateConfigurationRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateConfigurationRequest $request, $id)
    {
        $configuration = $this->configurationRepo->findConfiguration($id);
        $update = new ConfigurationRepository($configuration);
        $update->updateConfiguration($request->all());
        return response()->json($configuration);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $configuration = $this->configurationRepo->findConfiguration($id);
        $delete = new ConfigurationRepository($configuration);
        $delete->deleteConfiguration();
        return response()->json(['message' => 'Configuration deleted.'], 202);
    }
}