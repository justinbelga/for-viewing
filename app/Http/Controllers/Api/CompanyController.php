<?php
namespace AES\Http\Controllers\Api;

use AES\Api\Companies\Repositories\CompanyRepository;
use AES\Api\Companies\Repositories\Interfaces\CompanyRepositoryInterface;
use AES\Api\Companies\Requests\CreateCompanyRequest;
use AES\Api\Companies\Requests\UpdateCompanyRequest;
use Illuminate\Http\Request;
use AES\Http\Controllers\Controller;

class CompanyController extends Controller
{
    private $companyRepo;
    public function __construct(CompanyRepositoryInterface $companyRepository)
    {
        $this->companyRepo = $companyRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = $this->companyRepo->findAllCompanies();

        $paginate = $this->companyRepo->paginateArrayResults(collect($companies)->toArray());

        return response()->json($paginate);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateCompanyRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCompanyRequest $request)
    {
        $data = $request->all();
        $company = $this->companyRepo->createCompany($data);
        return response()->json($company, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $company = $this->companyRepo->findCompany($id);
        return response()->json($company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCompanyRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompanyRequest $request, $id)
    {
        $company = $this->companyRepo->findCompany($id);
        $update = new CompanyRepository($company);
        $update->updateCompany($request->all());
        return response()->json($company);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $company = $this->companyRepo->findCompany($id);
        $delete = new CompanyRepository($company);
        $delete->deleteCompany();
        return response()->json(['message' => 'Company deleted.'], 202);
    }
}