<?php

namespace AES\Http\Controllers\Api;

use AES\Api\Countries\Repositories\CountryRepository;
use AES\Api\Countries\Repositories\Interfaces\CountryRepositoryInterface;
use AES\Api\Countries\Requests\CreateCountryRequest;
use AES\Api\Countries\Requests\UpdateCountryRequest;
use Illuminate\Http\Request;
use AES\Http\Controllers\Controller;

class CountryController extends Controller
{
    private $countryRepo;

    public function __construct(CountryRepositoryInterface $countryRepository)
    {
        $this->countryRepo = $countryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = $this->countryRepo->findAllCountries();

        $paginate = $this->countryRepo->paginateArrayResults(collect($countries)->toArray());

        return response()->json($paginate);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateCountryRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCountryRequest $request)
    {
        $country = $this->countryRepo->createCountry($request->all());

        return response()->json($country, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $country = $this->countryRepo->findCountry($id);

        return response()->json($country);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCountryRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCountryRequest $request, $id)
    {
        $country = $this->countryRepo->findCountry($id);

        $update = new CountryRepository($country);
        $update->updateCountry($request->all());

        return response()->json($country);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $country = $this->countryRepo->findCountry($id);

        $delete = new CountryRepository($country);
        $delete->deleteCountry();

        return response()->json(['message' => 'Country deleted.'], 202);
    }
}
