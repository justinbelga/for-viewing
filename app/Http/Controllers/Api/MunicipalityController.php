<?php

namespace AES\Http\Controllers\Api;

use AES\Api\Municipalities\Repositories\MunicipalityRepository;
use AES\Api\Municipalities\Repositories\Interfaces\MunicipalityRepositoryInterface;
use AES\Api\Municipalities\Requests\CreateMunicipalityRequest;
use AES\Api\Municipalities\Requests\UpdateMunicipalityRequest;
use Illuminate\Http\Request;
use AES\Http\Controllers\Controller;

class MunicipalityController extends Controller
{
    private $municipalityRepo;

    public function __construct(MunicipalityRepositoryInterface $municipalityRepository)
    {
        $this->municipalityRepo = $municipalityRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $municipalities = $this->municipalityRepo->findAllMunicipalities();

        $paginate = $this->municipalityRepo->paginateArrayResults(collect($municipalities)->toArray());

        return response()->json($paginate);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateMunicipalityRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateMunicipalityRequest $request)
    {
        $municipality = $this->municipalityRepo->createMunicipality($request->all());
        return response()->json($municipality, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $municipality = $this->municipalityRepo->findMunicipality($id);
        return response()->json($municipality);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateMunicipalityRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMunicipalityRequest $request, $id)
    {
        $municipality = $this->municipalityRepo->findMunicipality($id);
        $update = new MunicipalityRepository($municipality);
        $update->updateMunicipality($request->all());
        return response()->json($municipality);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $municipality = $this->municipalityRepo->findMunicipality($id);
        $delete = new MunicipalityRepository($municipality);
        $delete->deleteMunicipality();
        return response()->json(['message' => 'Municipality deleted.'], 202);
    }
}