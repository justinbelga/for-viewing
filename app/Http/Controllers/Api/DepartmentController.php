<?php
namespace AES\Http\Controllers\Api;

use AES\Api\Departments\Repositories\DepartmentRepository;
use AES\Api\Departments\Repositories\Interfaces\DepartmentRepositoryInterface;
use AES\Api\Departments\Requests\CreateDepartmentRequest;
use AES\Api\Departments\Requests\UpdateDepartmentRequest;
use Illuminate\Http\Request;
use AES\Http\Controllers\Controller;

class DepartmentController extends Controller
{
    private $departmentRepo;
    public function __construct(DepartmentRepositoryInterface $departmentRepository)
    {
        $this->departmentRepo = $departmentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = $this->departmentRepo->findAllDepartments();

        $paginate = $this->departmentRepo->paginateArrayResults(collect($departments)->toArray());

        return response()->json($paginate);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateDepartmentRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateDepartmentRequest $request)
    {
        $data = $request->all();
        $department = $this->departmentRepo->createDepartment($data);
        return response()->json($department, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $department = $this->departmentRepo->findDepartmentById($id);
        return response()->json($department);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateDepartmentRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDepartmentRequest $request, $id)
    {
        $department = $this->departmentRepo->findDepartmentById($id);
        $update = new DepartmentRepository($department);
        $update->updateDepartment($request->all());
        return response()->json($department);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $department = $this->departmentRepo->findDepartmentById($id);
        $delete = new DepartmentRepository($department);
        $delete->deleteDepartment();
        return response()->json(['message' => 'Department deleted.'], 202);
    }
}